// ES6 UPDATES

// Exponent Operator - **
console.log(8 ** 2);
console.log(Math.pow(8, 2));

// Template Literals - ``
let myName = "Nelson";
let greeting = "Hello";
console.log(`${greeting} my name is ${myName}`);

let obj = {
  firstName: "John",
  lastName: "Smith",
  age: 0,
  emails: [],
};
let { firstName, lastName, age, ...rest } = obj;
console.log(rest);

// firstName = "Joe";

// console.log(firstName);
// console.log(obj.firstName);

const person = {
  firstName: "John",
  middleName: "Jacob",
  lastName: "Smith",
  age: 0,
};

let = { firstName, middleName, lastName } = person;

console.log(firstName);
console.log(middleName);
console.log(lastName);

let people = ["John", "Joe", "Jack"];
let firstPerson = person[0];
let secondPerson = person[1];
let thirdPerson = person[2];

// USING THE .keys() METHOD
console.log(Object.keys(person)); // getting each keys
console.log(Object.values(person)); // getting each value
Object.values(person).forEach((key) => console.log(typeof key)); // getting each typeof

// Arrow Function ()=>{} or 'implicit return statements'
const sayHello = () => {};
