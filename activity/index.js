// Session #19 Activity
// ES6

const getCube = (num) => {
  if (typeof num === "number") {
    let cube = num ** 3;
    console.log(`The cube of ${num} is ${cube}`);
  } else {
    console.log(`Wrong input! ${num} is not a number.`);
  }
};

getCube(2); // should be 8
getCube("e"); // NaN

let address = ["258 Washington Ave. NW", "California", "90011"];
let [homeAddress, state, zip] = address;
console.log(`I live at ${homeAddress}, ${state} ${zip}`);

let animal = {
  givenName: "Lolong",
  animalType: "salt water crocodile",
  weight: 1075, // in kilos
  measurement: 20.3, // ft
};
let { givenName, animalType, weight, measurement } = animal;

console.log(
  `${givenName} was a ${animalType}. He weighed at ${weight}kgs with a measurement of ${
    measurement.toString().split(".")[0]
  } feet and ${measurement.toString().split(".")[1]} inches.`
);

let arr_n = [1, 2, 3, 4, 5];

arr_n.forEach((n) => console.log(n));

let total = arr_n.reduce((prevValue, currentVlue) => prevValue + currentVlue);

console.log(total); // 15
